/**
 * redpen: a text inspection tool
 * Copyright (c) 2014-2015 Recruit Technologies Co., Ltd. and contributors
 * (see CONTRIBUTORS.md)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.redpen.parser.latex;

import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Experimental stream mode LaTeX parser prototype.
 */
public class StreamParser {
    private char[] mTarget;
    public static final String START = "ENVIRON_BEGIN";
    public static final String END = "ENVIRON_END";
    public static final String TEXTILE = "TEXTILE";
    private Listener mListener;

    public StreamParser(final char[] s, final Listener l) {
        mTarget = s;
        mListener = l;
    }

    public StreamParser(final String s, final Listener l) {
        mTarget = s.toCharArray();
        mListener = l;
    }

    public void parse() {
        final List<Token> tokens = Lexer.on(mTarget).parse();
        for (Token t:
                 (Parsing.unescapeRegion
                  (Parsing.styleTextileRegion
                   (Parsing.assembleRegion
                    (Parsing.pruneRegion
                     (Parsing.normalizeTextileRegion
                      (Parsing.maskTabularLikeRegion
                       (Parsing.markVerbatimRegion
                        (Parsing.collapse (tokens)))))))))) {
            mListener.element(t);
        }
    }

    /*package*/ static class Parsing {
        public static final char ESCAPE_CHAR = '\uFFFD';

        private Parsing() {
            throw new IllegalStateException("Parsing class");
        }

        public static List<Token> takeBlock(final Deque<Token> q) {
            final List<Token> o = new ArrayList<>();
            final Deque<String> reg = new ArrayDeque<>();
            try {
                while (true) {
                    final Token t = q.getFirst();
                    switch (t.getT()) {
                    case "GROUP1_BEGIN":
                    case "GROUP2_BEGIN":
                        reg.addLast(t.getT());
                    break;
                    default:
                        switch (t.getT()) {
                        case "GROUP1_END":
                            a(reg);
                            break;
                        case "GROUP2_END":
                            b(reg);
                            break;
                            default:
                                break;
                        }
                    }
                    if (!reg.isEmpty() || !o.isEmpty()) {
                        o.add(q.removeFirst());
                    }
                    if (reg.isEmpty()) {
                        break;
                    }
                }
                return o;
            } catch (final NoSuchElementException e) {
                return o;
            }
        }

        public static void a (final Deque<String> reg) {
            if ("GROUP1_BEGIN".equals(reg.getLast())) {
                reg.removeLast();
            }
        }

        public static void b (final Deque<String> reg) {
            if ("GROUP2_BEGIN".equals(reg.getLast())) {
                reg.removeLast();
            }
        }

        public static List<Token> takeTrailingBlocks(final Deque<Token> q) {
            final List<Token> o = new ArrayList<>();
            while (true) {
                final List<Token> block = takeBlock(q);
                if (!block.isEmpty()) {
                    o.addAll(block);
                } else {
                    break;
                }
            }
            return o;
        }

        public static Token makeVerbatim(final List<Token> tokens) {
            final Token beginning = tokens.get(0);
            final List<String> values = new ArrayList<>();
            for (Token t : tokens) {
                values.add(t.asVerbatim());
                if (!t.getP().isEmpty()) {
                    values.add(makeVerbatim(t.getP()).getV());
                }
            }
            return new Token("VERBATIM", StringUtils.join(values, ""), beginning.getPos());
        }


        public static List<Token> collapse(final List<Token> tokens) {
            final List<Token> o = new ArrayList<>();
            final Deque<Token> q = new ArrayDeque<>(tokens);
            final Pattern interests = Pattern.compile("(?:caption|part|(?:sub)*(chapter|section|paragraph)|item|title)\\*?");
            final Pattern ignores = Pattern.compile(".?(?:space|fill)\\*?|phantom|documentclass|usepackage|author|date|label|ref|cite|biblio.*|includegraphics|footnote");
            try {
                while (true) {
                    final Token t = q.removeFirst();
                    if ("CONTROL".equals(t.getT())) {
                        if ("begin".equals(t.getV()) || "end".equals(t.getV())) {
                            t.p = takeTrailingBlocks(q);
                            collapseSwitch(t);
                            o.add(t);
                        } else if (interests.matcher(t.v).matches()) {
                            t.p = takeBlock(q);
                            t.t = t.t + "*";
                            o.add(t);
                        } else if (ignores.matcher(t.v).matches()) {
                            t.p = takeTrailingBlocks(q);
                            o.add(t);
                        }
                        else if(q.isEmpty()) {
                            break;
                        }
                    } else {
                        o.add(t);
                    }
                }
                return o;
            } catch (final NoSuchElementException e) {
                return o;
            }
        }

        public static void collapseSwitch (Token t) {
            switch (t.v) {
                case "begin":
                    t.t = START;
                    break;
                case "end":
                    t.t = END;
                    break;
                default:
                    break;
            }
        }


        public static List<String> valuesOf(final Iterable<Token> tokens) {
            final List<String> o = new ArrayList<>();
            for (Token t : tokens) {
                o.add(t.v);
            }
            return o;
        }

        public static List<String> textileValuesOf(final Iterable<Token> tokens) {
            final List<String> o = new ArrayList<>();
            for (Token t : tokens) {
                o.add(t.asTextile());
            }
            return o;
        }

        public static List<Token> markVerbatimRegion(final List<Token> tokens) {
            final List<Token> o = new ArrayList<>();
            final Deque<Token> q = new ArrayDeque<>(tokens);
            try {
                while (true) {
                    final Token t = q.removeFirst();
                    if ( ! (START.equals(t.t) && textileValuesOf(t.p).contains("verbatim")) ) {
                        o.add(t);
                    } else {
                        final List<Token> verbatim = new ArrayList<>();
                        verbatimRegionLoop(o, q, verbatim);
                    }
                    if (q.isEmpty()){
                        break;
                    }
                }
                return o;
            } catch (final NoSuchElementException e) {
                return o;
            }
        }

        private static void verbatimRegionLoop(final List<Token> o, final Deque<Token> q, final List<Token> verbatim) {
            while (true) {
                final Token vt = q.removeFirst();
                if ( ! (END.equals(vt.t) && textileValuesOf(vt.p).contains("verbatim")) ) {
                    verbatim.add(vt);
                } else {
                    o.add(makeVerbatim(verbatim));
                    break;
                }
            }
        }

        public static List<Token> maskTabularLikeRegion(final List<Token> tokens) {
            final List<Token> o = new ArrayList<>();
            final Deque<Token> q = new ArrayDeque<>(tokens);
            try {
                while (true) {
                    final Token t = q.removeFirst();
                    if ( ! (START.equals(t.t) && textileValuesOf(t.p).contains("tabular")) ) {
                        o.add(t);
                    } else {
                        maskTabularLoop(q);
                    }
                    if (q.isEmpty()){
                        break;
                    }
                }
                return o;
            } catch (final NoSuchElementException e) {
                return o;
            }
        }

        private static void maskTabularLoop(final Deque<Token> q) {
            while (true) {
                final Token tt = q.removeFirst();
                if (END.equals(tt.t) && textileValuesOf(tt.p).contains("tabular")) {
                    break;
                }
            }
        }

        public static List<Token> normalizeTextileRegion(final List<Token> tokens) {
            final List<Token> o = new ArrayList<>();
            final Deque<Token> q = new ArrayDeque<>(tokens);
            try {
                while (true) {
                    final Token t = q.removeFirst();
                    if ( ! (TEXTILE.equals(t.t)) ) {
                        o.add(t);
                    } else {
                        t.v = Pattern.compile("``|’’|''").matcher(t.v).replaceAll("\"");
                        t.v = Pattern.compile("`|’").matcher(t.v).replaceAll("'");
                        o.add(t);
                    }
                    if (q.isEmpty()){
                        break;
                    }
                }
                return o;
            } catch (final NoSuchElementException e) {
                return o;
            }
        }

        public static List<Token> pruneRegion(final List<Token> tokens) {
            final List<Token> o = new ArrayList<>();
            final Deque<Token> q = new ArrayDeque<>(tokens);
            try {
                while (true) {
                        final Token t = q.removeFirst();
                        if (TEXTILE.equals(t.t)) {
                            o.add(t);
                        } else if ("CONTROL*".equals(t.t)) {
                            o.add(new Token(t.v.toUpperCase(), StringUtils.join(textileValuesOf(pruneRegion(t.p)), ""), t.pos));
                        }
                        if (q.isEmpty()){
                            break;
                        }
                    }
                    return o;
                } catch (final NoSuchElementException e) {
                return o;
            }
            }

        public static List<Token> assembleRegion(final List<Token> tokens) {
            final List<Token> o = new ArrayList<>();
            final Deque<Token> q = new ArrayDeque<>(tokens);
            final List<Token> reg = new ArrayList<>();
            try {

                do {
                    final Token t = q.removeFirst();
                    if (TEXTILE.equals(t.t)) {
                        reg.add(t);
                    } else {
                        if (!reg.isEmpty()) {
                            final Token first = reg.get(0);
                            o.add(new Token(first.t, StringUtils.join(textileValuesOf(reg), ""), first.pos));
                            reg.clear();
                        }
                        o.add(t);
                    }
                } while (true);
            } catch (final NoSuchElementException e) {
                if (!reg.isEmpty()) {
                    final Token first = reg.get(0);
                    o.add(new Token(first.t, StringUtils.join(textileValuesOf(reg), ""), first.pos));
                    reg.clear();
                }
                return o;
            }
        }

        public static List<Token> styleTextileRegion(final List<Token> tokens) {
            final List<Token> o = new ArrayList<>();
            final Deque<Token> q = new ArrayDeque<>(tokens);
            final Pattern empty = Pattern.compile(String.format("^[ \\t\\r%n%c]*$", ESCAPE_CHAR));
            final Pattern lineBreak = Pattern.compile("(\\r?\\n){2,}");
            try {
                while (true) {
                    final Token t = q.removeFirst();
                    if (TEXTILE.equals(t.t) && !empty.matcher(t.v).matches()) {
                            final String stripped = stripTextBlock(t.v);
                            for (String s : lineBreak.split(stripped)) {
                                final Position p = new Position(guessRow(t, s), guessCol(t, s));
                                o.add(new Token(t.t, maskCharactersInTextBlock(s), p));
                                o.add(new Token(t.t, Token.BLANK_LINE, p));
                            }
                    } else {
                        o.add(t);
                    }
                }
            } catch (final NoSuchElementException e) {
                try {
                    final Token last = o.get(o.size() - 1);
                    if (last.isBlankLine()) {
                        o.remove(last);
                    }
                    return o;
                } catch (final IndexOutOfBoundsException ignore) {
                    return o;
                }
            }
        }

        public static String stripTextBlock(final String b) {
            return Pattern.compile(String.format("^[ \\t\\r%n%c]+", ESCAPE_CHAR)).matcher(b).replaceAll("");
        }

        public static String compactTextBlock(final String b) {
            return Pattern.compile(String.format("[ \\t\\r%n%c]{2,}", ESCAPE_CHAR)).matcher(b).replaceAll(" ");
        }

        public static String maskCharactersInTextBlock(final String b) {
            return b.replace('~', ' ');
        }

        public static List<Token> unescapeRegion(final List<Token> tokens) {
            final List<Token> o = new ArrayList<>();
            for (Token t : tokens) {
                t.v = t.v.replace(ESCAPE_CHAR, ' ');
                o.add(t);
            }
            return o;
        }

        public static int countMatches(final Pattern p, final String s) {
            int ret = 0;
            for (Matcher m = p.matcher(s); m.find(); ++ret);
            return ret;
        }

        public static int guessRow(final Token t, final String needle) {
            final int lead = t.v.indexOf(needle);
            if (lead >= 0) {
                return t.pos.getRow() + countMatches(Pattern.compile("\\r?\\n"), t.v.substring(0, lead));
            } else {
                return t.pos.getRow();
            }
        }

        public static int guessCol(final Token t, final String needle) {
            final int lead = t.v.indexOf(needle);
            if (lead >= 0) {
                final int lastLinebreak = t.v.substring(0, lead).lastIndexOf('\n');
                if (lastLinebreak >= 0) {
                    return (lead - (lastLinebreak + 1));
                } else {
                    return t.pos.getCol() + lead;
                }
            } else {
                return t.pos.getCol();
            }
        }
    }


    public static interface Listener {
        public void element(Token t);
    }
}
