/**
 * redpen: a text inspection tool
 * Copyright (c) 2014-2015 Recruit Technologies Co., Ltd. and contributors
 * (see CONTRIBUTORS.md)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.redpen.parser.latex;

import java.util.List;
import java.util.ArrayList;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Token data type.
 */
public class Token {
    public static final String BLANK_LINE = "";

    protected String t;
    protected String v;
    protected List<Token> p = new ArrayList<>();
    protected Position pos;

    public static String getBlankLine() {
        return BLANK_LINE;
    }

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public List<Token> getP() {
        return p;
    }

    public void setP(List<Token> p) {
        this.p = p;
    }

    public Position getPos() {
        return pos;
    }

    public void setPos(Position pos) {
        this.pos = pos;
    }

    public Token(final String type, final String value, final Position pos) {
        this.t = type;
        this.v = value;
        this.pos = new Position(pos);
    }

    public Token(final String type, final char value, final Position pos) {
        this.t = type;
        this.v = String.valueOf(value);
        this.pos = new Position(pos);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Token)) return false;
        Token token = (Token) o;
        return Objects.equals(t, token.t) &&
                Objects.equals(v, token.v) &&
                Objects.equals(p, token.p) &&
                Objects.equals(pos, token.pos);
    }

    @Override
    public int hashCode() {
        return Objects.hash(t, v, p, pos);
    }

    public boolean equals(final Token other) {
        return (this.t == other.t && this.v == other.v && this.p.equals(other.p));
    }



    @Override
    public String toString() {
        return String.format("%s(%s) \"%s\" %s", this.t, this.pos, this.v, this.p);
    }

    public String asTextile() {
        if ("TEXTILE".equals(t) || t.endsWith("*")) {
            return v;
        } else {
            return "";
        }
    }

    public String asVerbatim() {
        return v;
    }

    public boolean isEmptyAsTextile() {
        return Pattern.compile("^[ \\r\\n\\t]*$").matcher(asTextile()).matches();
    }

    public boolean isBlankLine() {
        return ("TEXTILE".equals(t) && "".equals(v));
    }
}
