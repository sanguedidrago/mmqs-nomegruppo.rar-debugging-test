/**
 * redpen: a text inspection tool
 * Copyright (c) 2014-2015 Recruit Technologies Co., Ltd. and contributors
 * (see CONTRIBUTORS.md)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.redpen.parser.review;

import cc.redpen.parser.PreprocessingReader;
import cc.redpen.parser.common.Line;
import cc.redpen.parser.common.LineParser;
import cc.redpen.parser.common.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ReVIEWParser extends LineParser {
    private static final Logger LOG = LoggerFactory.getLogger(ReVIEWParser.class);

    /**
     * current parser state
     */
    private class State {
        // are we in a block
        private boolean inBlock = false;
        // are we in a list?
        private boolean inList = false;
        // should we erase lines within the current block?
        private boolean eraseBlock = true;
        // the sort of block we are in
        private String type;

        public boolean isInBlock() {
            return inBlock;
        }

        public void setInBlock(boolean inBlock) {
            this.inBlock = inBlock;
        }

        public boolean isInList() {
            return inList;
        }

        public void setInList(boolean inList) {
            this.inList = inList;
        }

        public boolean isEraseBlock() {
            return eraseBlock;
        }

        public void setEraseBlock(boolean eraseBlock) {
            this.eraseBlock = eraseBlock;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    class ReVIEWBlock {
        private String type = "";
        private List<String> properties = new ArrayList<>();
        private boolean isOpen = false;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public List<String> getProperties() {
            return properties;
        }

        public void setProperties(List<String> properties) {
            this.properties = properties;
        }

        public boolean isOpen() {
            return isOpen;
        }

        public void setOpen(boolean open) {
            isOpen = open;
        }
    }

    static Pattern digitPattern = Pattern.compile("^\\s+[0-9]+\\.");

    protected void populateModel(Model model, InputStream io) {
        State state = new State();
        int lineno = 0;

        try (PreprocessingReader reader = createReader(io)) {
            // add the lines to the model
            while (true) {
                String line = reader.readLine();
                if (line == null) {
                    break;
                }
                lineno++;
                model.add(new ReVIEWLine(line, lineno));
            }

            model.setPreprocessorRules(reader.getPreprocessorRules());

            for (model.rewind(); model.isMore(); model.getNextLine()) {
                processLine(model.getCurrentLine(), model, state);
            }
        } catch (Exception e) {
            LOG.error("context", e);
            LOG.error("Exception when parsing Re:VIEW file", e);
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("Re:VIEW parser model (X=erased line,[=block,section-listlevel-lineno,*=list item):{}", model);
        }
    }

    private void processLine(Line line, Model model, State state) {
        if (line.isErased()) { return; }

        TargetLine target = new TargetLine(line,
                model.getLine(line.getLineNo() - 1),
                model.getLine(line.getLineNo() + 1));

        if (state.inList && (line.getListLevel() == 0)) {
            line.setListLevel(target.getPreviousLine().getListLevel());
        }

        // check for block end
        if (state.inBlock) {
            if (target.getLine().startsWith("//}") && target.getLine().length() == 3) {
                state.inBlock = false;
            }
            line.erase();
        }

        // test for various block starts
        if ((!state.inBlock) && target.getLine().startsWith("//")) {
            ReVIEWBlock block = parseBlock(line);
            line.erase();
            state.inBlock = block.isOpen;
        }

        // handling comments
        if (target.getLine().startsWith("#@#")) {
            line.erase();
        }

        // enclosed inline markups
        line.eraseEnclosure("@<list>{", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<code>{", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<img>{", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<table>{", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<fn>{", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<chap>{", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<title>{", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<chapref>{", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<bou>{", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<kw>{", "}", Line.EraseStyle.PRESERVE_AFTER_LABEL);
        line.eraseEnclosure("@<chapter>{", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<ruby>{", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<ami>{", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<b>{", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<i>{", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<strong>{", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<em>{", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<tt>{", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<tti>{", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<ttb>{", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<u>{", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<br>{", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<m>{", "}", Line.EraseStyle.ALL);
        line.eraseEnclosure("@<icon>{", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<uchar>{", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<href>{", "}", Line.EraseStyle.PRESERVE_LABEL);
        line.eraseEnclosure("@<column>{", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<raw>{|html|", "}", Line.EraseStyle.ALL);
        line.eraseEnclosure("@<raw>{|latex|", "}", Line.EraseStyle.MARKERS);
        line.eraseEnclosure("@<raw>{|idgxml|", "}", Line.EraseStyle.ALL);
        line.eraseEnclosure("@<raw>{|top|", "}", Line.EraseStyle.ALL);

        //opening annotation for preprocessor
        line.eraseEnclosure("#@warn(", ")", Line.EraseStyle.ALL);
        line.eraseEnclosure("@comment(", ")", Line.EraseStyle.ALL);
        line.eraseEnclosure("#@mapfile(", ")", Line.EraseStyle.ALL);
        line.eraseEnclosure("#@maprange(", ")", Line.EraseStyle.ALL);
        line.eraseEnclosure("#@mapoutput(", ")", Line.EraseStyle.ALL);

        lineHandling(line, state, target);
    }

    /**
     * Does the give line start a list?
     *
     * @param line
     * @return
     */
    private boolean isListElement(Line line, Line nextLine) {
        if (isNormalList(line)) return true;

        // is numbered list?
        if (isDigitList(line)) return true;

        // is labeled list?
        return isLabeledList(line, nextLine);
    }

    private void lineHandling (Line line, State state, TargetLine target) {
        //closing annotation for preprocessor
        closeAnnotation(line, target);

        // handling section
        int headerIndent = 0;
        while (line.charAt(headerIndent) == '=') {
            headerIndent++;
        }
        int cursor = headerIndent;
        if (line.charAt(cursor) == '[') { // column?
            while (line.charAt(cursor) != ']'
                    && line.charAt(cursor) != 0) {
                cursor++;
            }
            cursor++;
        } else if (line.charAt(cursor) == '{') { // label?
            while (line.charAt(cursor) != '}'
                    && line.charAt(cursor) != 0) {
                cursor++;
            }
            cursor++;
        }
        if ((cursor > 0) && (line.charAt(cursor) == ' ')) {
            line.erase(0, cursor + 1);
            line.setSectionLevel(cursor);
        }
        if (target.getLine().startsWith("===[/column]")) { // closing..
            line.erase();
        }

        // list
        handlingList(line, state, target);
    }

    private void closeAnnotation (Line line, TargetLine target) {
        if (target.getLine().startsWith("#@end")) {
            line.erase();
        }
    }

    private void handlingList (Line line, State state, TargetLine target) {
        if (!state.inBlock && isListElement(line, target.getNextLine())) {
            state.inList = true;
        }

        // a blank line will cancel any list element we are in
        if (state.inList && (line.length() == 0)) {
            state.inList = false;
            line.setListLevel(0);
        }
    }

    private boolean isLabeledList(Line line, Line nextLine) {
        int spacePos = 0;
        while(' ' == line.charAt(spacePos)) { spacePos++; }

        if (line.charAt(spacePos) == ':') {
            nextLine.setListLevel(1);
            nextLine.setListStart(true);
            line.erase();
            return true;
        }
        return false;
    }

    private boolean isDigitList(Line line) {
        Matcher m = digitPattern.matcher(line.getText());
        if (m.find()) {
            int dotPos = line.getText().indexOf('.');
            line.setListLevel(1);
            line.setListStart(true);
            line.erase(0, dotPos);
            while (Character.isWhitespace(line.charAt(dotPos))) {
                line.erase(dotPos, 1);
                dotPos++;
            }
            return true;
        }
        return false;
    }

    private boolean isNormalList(Line line) {
        int pos = 0;
        while(' ' == line.charAt(pos)) { pos++; }
        if (pos == 0) { return false; }

        // is the first non-space character a suitable list marker?
        if ("*".indexOf(line.charAt(pos)) != -1) {
            char listMarker = line.charAt(pos);
            int level = 1; pos++;
            while (line.charAt(pos) == listMarker) {
                pos++;
                level++;
            }

            if (Character.isWhitespace(line.charAt(pos))) {
                line.setListLevel(level);
                line.setListStart(true);
                line.erase(0, pos);
                while (Character.isWhitespace(line.charAt(pos))) {
                    line.erase(pos, 1);
                    pos++;
                }
                return true;
            }
        }
        return false;
    }

    ReVIEWBlock parseBlock(Line line) {
        ReVIEWBlock block = new ReVIEWBlock();
        String text = line.getText();
        // detect type
        int openIdx = text.indexOf('[');
        if (openIdx > 0) {
            block.type = text.substring(2, openIdx);
            // detect properties
            int closeIdx = text.indexOf(']');
            while(closeIdx > 0) {
                block.getProperties().add(text.substring(openIdx+1, closeIdx));
                openIdx = text.indexOf('[', openIdx+1);
                closeIdx = text.indexOf(']', closeIdx+1);
            }
        } else {
            int leftBracePosition = text.indexOf('{');
            block.type = text.substring(2, leftBracePosition > 0 ? leftBracePosition : text.length() - 1);
        }

        // detect open block
        if (text.contains("{")) {
            block.isOpen = true;
        }
        return block;
    }
}
