/**
 * redpen: a text inspection tool
 * Copyright (c) 2014-2015 Recruit Technologies Co., Ltd. and contributors
 * (see CONTRIBUTORS.md)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.redpen.parser.rest;

import cc.redpen.parser.PreprocessingReader;
import cc.redpen.parser.common.Line;
import cc.redpen.parser.common.LineParser;
import cc.redpen.parser.common.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static cc.redpen.parser.rest.MultiLineProcessUtils.processMultiLineMatch;

public class ReSTParser extends LineParser {
    private static final Logger LOG = LoggerFactory.getLogger(ReSTParser.class);

    static Pattern digitPattern = Pattern.compile("^\\s*[0-9#]+\\.");
    static Pattern normalTablePattern = Pattern.compile("^[+][-+]+[+]$");
    static Pattern csvTablePattern = Pattern.compile("^=+[= ]+=$");
    static Pattern directivePattern = Pattern.compile("^[.][.] [a-z]+::");
    static Pattern inlineCommentPattern = Pattern.compile("^[.][.] [^\\[][^:]+$");
    static Pattern footnotePattern = Pattern.compile("^[.][.] \\[.+\\].+");
    /**
     * current parser state
     */
    private class State {
        // are we in table
        private boolean inTable = false;
        // are we in a block such as directives, comments and so on
        private boolean inBlock = false;
        // are we in a list?
        private boolean inList = false;
    }

    @Override
    protected void populateModel(Model model, InputStream io) {
        State state = new State();
        int lineno = 0;
        try (PreprocessingReader reader = createReader(io)) {
            // add the lines to the model
            while (true) {
                String line = reader.readLine();
                if (line == null) {
                    break;
                }
                lineno++;
                model.add(new ReSTLine(line, lineno));
            }

            model.setPreprocessorRules(reader.getPreprocessorRules());
            for (model.rewind(); model.isMore(); model.getNextLine()) {
                processLine(model.getCurrentLine(), model, state);
            }
        } catch (Exception e) {
            LOG.error("Exception when parsing reST file", e);
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("reST parser model (X=erased line,[=block,section-listlevel-lineno,*=list item): {}", model);
        }
    }

    private void processLine(Line line, Model model, State state) {
        if (line.isErased()) { return; }
        TargetLine target = new TargetLine(line,
                model.getLine(line.getLineNo() - 1),
                model.getLine(line.getLineNo() + 1));

        // handle section
        int level = extractSectionLevel(target);
        if (level > 0) {
            line.setSectionLevel(level);
            reset(state);
        }

        // handle new line style line (see https://stackoverflow.com/questions/7033239/how-to-preserve-line-breaks-when-generating-python-docs-using-sphinx)
        if ((target.getFirstChar() == '|') && (target.getSecondChar() == ' ')) {
            line.erase(0, 2);
        }

        // handle inline markups
        this.eraseInlineMarkup(line);

        // handle list (bullets, definition...)
        if (!state.inBlock && isListElement(target, state)) { state.inList = true; }

        // handle table (normal, csv)
        if (!state.inBlock && isTable(target, state)) { state.inTable = true; }

        // handle directives (image, raw, contents...)
        if (isDirective(target)) { state.inBlock = true; }

        // handle source codes (literal blocks)
        if (isLiteral(target)) { state.inBlock = true; }

        // handle comments
        if (isComment(target)) { state.inBlock = true; }

        // handle line block
        handlelineBlock(line); //NOTE: line block does not effect upcoming lines

        // handle footnotes
        handleFootnote(line);

        // a blank line will reset any blocks element we are in
        if (isEndBlock(target)) { reset(state); }

        // lines in block is not checked
        if (state.inBlock) { line.erase(); }
    }

    private void handleFootnote(Line line) {
        Matcher m = footnotePattern.matcher(line.getText());
        if (m.find()) {
            line.erase(0, line.getText().indexOf(']')+2);
        }
    }

    private void handlelineBlock(Line line) {
        if (line.charAt(0) == '|' && line.charAt(1) == ' ' && line.startsWith(">>>"))  {
            line.erase();
        }
    }

    private boolean isEndBlock(TargetLine target) {
        // not continue indent in the next line
        return target.getLine().length() == 0 &&
                ((target.getNextLine().charAt(0) != ' ' && target.getNextLine().charAt(1) != ' ')
                        && target.getNextLine().charAt(0) != '\t');
    }

    private boolean isLiteral(TargetLine target) {
        Line line = target.getLine();
        return line.length() == 2 && (line.charAt(0) == ':' && line.charAt(1) == ':')
                && target.getNextLine().length() == 0;
    }

    private boolean isComment(TargetLine target) {
        Line line = target.getLine();

        // check if inline comment start?
        Matcher m = inlineCommentPattern.matcher(target.getLine().getText());
        if (m.find()) {
            target.getLine().erase();
            return true;
        }

        // check if block comment start?
        return line.length() == 2 && (line.charAt(0) == '.' && line.charAt(1) == '.');
    }

    private boolean isDirective(TargetLine target) {
        Matcher m = directivePattern.matcher(target.getLine().getText());
        if (m.find()) {
            target.getLine().erase();
            return true;
        }
        return false;
    }

    private void reset(State state) {
        state.inList = false;
        state.inBlock = false;
        state.inTable = false;
    }

    private boolean isTable(TargetLine target, State state) {
        Line line = target.getLine();
        if (state.inTable) {
            line.erase();
            return true;
        }

        if (isNormalTable(target)) return true;
        return isCSVTable(target);
    }

    private boolean isNormalTable(TargetLine target) {
        Matcher m = normalTablePattern.matcher(target.getLine().getText());
        if (m.find()) {
            target.getLine().erase();
            return true;
        }
        return false;
    }

    private boolean isCSVTable(TargetLine target) {
        Matcher m = csvTablePattern.matcher(target.getLine().getText());
        if (m.find()) {
            target.getLine().erase();
            return true;
        }
        return false;
    }


    private boolean isListElement(TargetLine line, State state) {
        if (isNormalList(line)) return true;
        if (isDigitList(line)) return true;
        return isDefinitionList(line, state);
    }

    private boolean isNormalList(TargetLine target) {
        Line line = target.getLine();
        int spacePos = 0;
        while(' ' == line.charAt(spacePos)) { spacePos++; }
        if (line.charAt(spacePos) == '*') {
            line.setListLevel(1);
            line.setListStart(true);
            line.erase(0, spacePos+1);
            removeWhitespaces(line, ++spacePos);
            return true;
        }
        return false;
    }

    private boolean isDefinitionList(TargetLine target, State state) {
        Line line = target.getLine();
        Line nextLine = target.getNextLine();

        // handling block tag
        if (
                (line.charAt(0) != ' ' && line.charAt(1) != ' ' && line.charAt(0) != '\t') && // not start from indents
                (line.charAt(line.length() - 1) != ':' && line.charAt(line.length() - 2) != ':') && // not source code
                        (nextLine.charAt(0) == ' ' && (nextLine.charAt(1) == ' ') || nextLine.charAt(0) == '\t') // have indentation in next line
                )
        {
            line.erase();
            return true;
        }

        // handling list contents
        if (state.inList && ((line.charAt(0) == ' ' && line.charAt(1) == ' ')  || (line.charAt(0) == '\t'))) {
            line.setListLevel(1);
            line.setListStart(true);
            removeWhitespaces(line, 0);
            return true;
        }
        return false;
    }

    private boolean isDigitList(TargetLine target) {
        Line line = target.getLine();
        Matcher m = digitPattern.matcher(target.getLine().getText());
        if (m.find()) {
            line.setListLevel(1);
            line.setListStart(true);
            int dotPos = target.getLine().getText().indexOf('.');
            line.erase(0, ++dotPos);
            removeWhitespaces(line, dotPos);
            return true;
        }
        return false;
    }

    private int extractSectionLevel(TargetLine target) {
        if (processMultiLineMatch('#', '#', target)
                || processMultiLineMatch('*', '*', target)
                || processMultiLineMatch('=', '=', target)
                || processMultiLineMatch(null, '=', target)
                || processMultiLineMatch('-', '-', target)
                || processMultiLineMatch(null, '-', target)
                || processMultiLineMatch('~', '~', target)
                || processMultiLineMatch(null, '~', target)
                || processMultiLineMatch('^', '^', target)
                || processMultiLineMatch(null, '^', target)) {
            return 1;
        }
        return -1;
    }

    /**
     * Erase all inline markup (bold, italics, special tokens etc)
     *
     * @param line
     */
    private void eraseInlineMarkup(Line line) {
        // inline markup (bold, italics etc)
        line.eraseEnclosure(":ref:`", "`", ReSTLine.EraseStyle.INLINE_MARKUP); // inline cross section reference
        line.eraseEnclosure("`", "`:sup:", ReSTLine.EraseStyle.INLINE_MARKUP); // superscript
        line.eraseEnclosure("`", "`sub:", ReSTLine.EraseStyle.INLINE_MARKUP); // subscript
        line.eraseEnclosure("*", "*", ReSTLine.EraseStyle.INLINE_MARKUP); // emphasis
        line.eraseEnclosure("**", "**", ReSTLine.EraseStyle.INLINE_MARKUP); // strong emphasis
        line.eraseEnclosure("`", "`", ReSTLine.EraseStyle.INLINE_MARKUP); // interpreted text
        line.eraseEnclosure("``", "``", ReSTLine.EraseStyle.INLINE_MARKUP); // inline literal
        line.eraseEnclosure("`", "`_", ReSTLine.EraseStyle.INLINE_MARKUP); // phrase reference
        line.eraseEnclosure("_`", "`", ReSTLine.EraseStyle.INLINE_MARKUP); // inline literal target
        line.eraseEnclosure("[", "]_", ReSTLine.EraseStyle.ALL); // footnote reference
        line.eraseEnclosure("|", "|", ReSTLine.EraseStyle.INLINE_MARKUP); // inline figure

    }

    private void removeWhitespaces(Line line, int startPosition) {
        while (Character.isWhitespace(line.charAt(startPosition))) {
            line.erase(startPosition, 1);
            startPosition++;
        }
    }
}
