package cc.redpen.util;

public class Pair<A, B> {
    private A first;
    private B second;

    public A getFirst() {
        return first;
    }

    public void setFirst(A first) {
        this.first = first;
    }

    public B getSecond() {
        return second;
    }

    public void setSecond(B second) {
        this.second = second;
    }

    public Pair(A f, B s) {
        this.first=f;
        this.second=s;
    }
}
