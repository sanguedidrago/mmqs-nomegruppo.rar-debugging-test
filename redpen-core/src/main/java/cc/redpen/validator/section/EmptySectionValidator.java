package cc.redpen.validator.section;

import cc.redpen.RedPenException;
import cc.redpen.model.Paragraph;
import cc.redpen.model.Section;
import cc.redpen.model.Sentence;
import cc.redpen.validator.Validator;

import java.util.Objects;
import java.util.Optional;
/**
 * redpen: a text inspection tool
 * Copyright (c) 2014-2015 Recruit Technologies Co., Ltd. and contributors
 * (see CONTRIBUTORS.md)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class EmptySectionValidator extends Validator {
    private int sectionLevelLimit = 5;

    @Override
    public void validate(Section section) {
        if (0 == section.getLevel() || section.getLevel() >= sectionLevelLimit) {
            return;
        }

        Sentence header = section.getJoinedHeaderContents();
        if (section.getNumberOfParagraphs() == 0) {
            addLocalizedError(header, header.getContent());
        } else {
            paragraphLoop(section, header);
        }
    }

    @Override
    protected void init() throws RedPenException {
        Optional<String> limit = getConfigAttribute("limit");
        if (limit.isPresent()) {
            sectionLevelLimit = Integer.parseInt(limit.get());
        }
    }

    public void paragraphLoop (Section section, Sentence header) {
        for (Paragraph p : section.getParagraphs()) {
            if (p.getNumberOfSentences() == 0) {
                addLocalizedError(header, header.getContent());
                break;
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EmptySectionValidator)) return false;
        if (!super.equals(o)) return false;
        EmptySectionValidator that = (EmptySectionValidator) o;
        return sectionLevelLimit == that.sectionLevelLimit;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), sectionLevelLimit);
    }
}
