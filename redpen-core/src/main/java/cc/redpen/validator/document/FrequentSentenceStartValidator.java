/**
 * redpen: a text inspection tool
 * Copyright (c) 2014-2015 Recruit Technologies Co., Ltd. and contributors
 * (see CONTRIBUTORS.md)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cc.redpen.validator.document;

import cc.redpen.model.Document;
import cc.redpen.model.Paragraph;
import cc.redpen.model.Sentence;
import cc.redpen.validator.Validator;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Check that too many sentences don't start with the same words
 */
public class FrequentSentenceStartValidator extends Validator {
    private static final String LEADING = "leading_word_limit";
    private Map<String, Integer> sentenceStartHistogram = new HashMap<>(); // histogram of sentence starts

    public FrequentSentenceStartValidator() {
        super(LEADING, 3, // number of words starting each sentence to consider
              "percentage_threshold", 25, // maximum percentage of sentences that can start with the same words
              "min_sentence_count", 5); // must have at least this number of sentences
    }

    /**
     * Add sequences of tokens, up to leadingWordLimit, in the histogram
     */
    private void processSentence(Sentence sentence) {
        if (sentence.getTokens().size() > getInt(LEADING)) {
            StringBuilder leadingPhrase = new StringBuilder();
            for (int i = 0; i < getInt(LEADING); i++) {
                final String s = leadingPhrase.append(String.valueOf(leadingPhrase).isEmpty() ? "" : " ")
                        + sentence.getTokens().get(i).getSurface();
                Integer count = sentenceStartHistogram.get(leadingPhrase);
                if (sentenceStartHistogram.get(leadingPhrase) == null) {
                    count = 0;
                }
                sentenceStartHistogram.put(s, count + 1);
            }
        }
    }

    @Override
    public void validate(Document document) {
        // remember the last sentence since we can't add an error without a sentence
        Sentence lastSentence = null;
        int sentenceCount = 0;
        for (int i = 0; i < document.size(); i++) {
            for (Paragraph para : document.getSection(i).getParagraphs()) {
                for (Sentence sentence : para.getSentences()) {
                    processSentence(sentence);
                    sentenceCount++;
                    lastSentence = sentence;
                }
            }
        }

        // make sure we have enough sentences to make this validation worthwhile
        if (sentenceCount >= getInt("min_sentence_count")) {
            for (Map.Entry<String, Integer> start : sentenceStartHistogram.entrySet()) {
                int count = sentenceStartHistogram.get(start.getKey());
                int percentage = (int) (100.0 * (float) count / (float) sentenceStartHistogram.size());
                if (percentage > getInt("percentage_threshold")) {
                    addLocalizedError("SentenceStartTooFrequent", lastSentence, percentage, start.getKey());
                }
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        FrequentSentenceStartValidator that = (FrequentSentenceStartValidator) o;
        return Objects.equals(sentenceStartHistogram, that.sentenceStartHistogram);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), sentenceStartHistogram);
    }
}
