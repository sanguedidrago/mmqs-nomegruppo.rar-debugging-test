package cc.redpen.validator.sentence;

import cc.redpen.model.Sentence;
import cc.redpen.validator.Validator;

public class NumberOfCharactersValidator extends Validator {

    @Override
    public void validate(Sentence sentence) {
        int minLength = 100;
        if (sentence.getContent().length() < minLength) {
            addError("Sentence is shorter than " + minLength + " characters long.", sentence);
        }
        int maxLength = 1000;
        if (sentence.getContent().length() > maxLength) {
            addError("Sentence is longer than " + maxLength + " characters long.", sentence);
        }
    }
}
